*** Settings ***
#Library    SeleniumLibrary
Resource    ../Resources/resources_BioServ.robot

Suite Setup
Suite Teardown

*** Test Cases ***
Viewing Biographic Details
    [Setup]    Launch Browser
    Login as Student
    Open Biographical Details
    Verify Biographical Details
    Logout as Student
    [Teardown]    close browser
