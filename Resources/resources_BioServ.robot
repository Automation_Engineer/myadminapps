*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${url}  http://myadminqa.int.unisa.ac.za/portal/xlogin
${browser}  headlessfirefox
${username}  24836133
${password}    Testing123
${LoginBtn}     /html/body/unisa-root/unisa-credentials-input/form/div[1]/button
${title}    xpath=//body//div[@class='container']//div[@class='container']//div[1]//div[1]

*** Keywords ***
Launch Browser
    open browser    ${url}  ${browser}
    maximize browser window
    #set selenium speed    2

Login as Student
    input text    name:eid      ${username}
    input text    name:pw       ${password}
    click element   id:submit

Open Biographical Details
    #sleep    10
    #wait until page contains  Sites
    set selenium speed    5
    click element    class:all-sites-label
    log to console    Click all Sites button
    #sleep    2
    click element    xpath://body//div[@id='otherSitesCategorWrap']//div//div[3]//ul[1]//li[2]//div[1]//a[1]//span[1]
    #sleep    2
    click element   xpath://li[5]//a[1]//span[2]
    log to console  Click myAdmin
    click element   xpath://li[5]//a[1]//span[2]
    wait until page contains    Biographical Details
    log to console  Biographical Details page opens Successfuly
    #sleep    5

Verify Biographical Details
    ${text as list}=  Get Text  //body
    Log       ${text as list}    # a very long string, with newlines as delimiters b/n the different tags

Logout as Student
    click element    id:loginUser
    click element    xpath://span[@class='Mrphs-login-Message']





