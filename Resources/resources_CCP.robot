*** Settings ***
Library    SeleniumLibrary
Resource    ../Resources/cardDetails.robot

*** Variables ***
${url}      https://myadminqa.int.unisa.ac.za/services/
${browser}  headlessfirefox
${studentNum}    8059454
${password}     Testing123
${email}        tmaimakm@unisa.ac.za
${expected_Text}    Please check your qualification, change it if necessary and choose Continue.
${expected_Text2}   Complete the personal and payment details below and choose Pay Now.
${amount_paid}      65.00
${payNow}       //body//button[1]
${expected_Text3}   Please capture your card details below:
${card_Holder}  Mr Automation Engineer
${payNow2}      //button[@id='pay']
${code}     test123

*** Keywords ***
Launch Browser
    open browser    ${url}  ${browser}
    maximize browser window
    capture page screenshot    screenshots/CreditCardPayment/Landing Page.png

Access Credit Card Payment App
    click element    xpath:/html/body/nav/div/div[2]/ul/li[3]/a
    log to console    Click Student Finance dropdown
    click element    xpath:/html/body/nav/div/div[2]/ul/li[3]/ul/li[3]/a
    log to console    Click the Credit Card Payment Option
    sleep    10
    input text      id:studentNumber    ${studentNum}
    capture page screenshot    screenshots/CreditCardPayment/EnterStuNumber.png
    sleep    10
    click element    xpath:/html/body/div/unisa-root/unisa-student-number-input/div/form/div[3]/button  #click continue
    log to console    Click continue
    sleep    10
    wait until page contains    ${expected_Text}
    capture page screenshot    screenshots/CreditCardPayment/qualification page.png
    log to console    Verify the students qualification page
    click element    xpath:/html/body/div/unisa-root/unisa-qual-input/div/form/div[3]/button[1]     #click continue
    log to console    Student is successfully directed to the Payment page
    capture page screenshot    screenshots/CreditCardPayment/Access Credit Card Payment App.png
    sleep    5

Complete the Personal and Payment Details
    wait until page contains    ${expected_Text2}
    input text    id:email      ${email}
    log to console    Add email address
    sleep    5
    select checkbox     payLibraryFee
    log to console    Select the checkbox
    sleep    5
    input text    id:ccTotalAmountInput     ${amount_paid}
    log to console    Add total amount payable
    click element    xpath:${payNow}
    log to console  Click the Pay Now button
    capture page screenshot    screenshots/CreditCardPayment/Complete the Personal and Payment Details.png

Capture Card Details
    sleep    10
    wait until page contains    ${expected_Text3}
    log to console    Verify Card Details page
    input text  id:name     ${card_Holder}
    log to console    Add Name on Card
    input text  id:cardNumber   ${MasterCardReg}
    log to console    Add Card Number
    select from list by value    expiryMonth    ${MCRegEM}
    log to console    Add Expiration Month
    select from list by value    expiryYear     ${MCRegEY}
    log to console    Add Expiration Year
    select radio button    budget   false
    log to console    Select Radio button option
    input text    id:securityCode   ${MCRegCVV}
    log to console    Add Security Code
    capture page screenshot    screenshots/CreditCardPayment/Capture Card Details.png
    click element    xpath:${payNow2}
    log to console    Click Pay Now
    sleep    10

Authenticate using password
    input text    code  ${code}
    capture page screenshot    screenshots/CreditCardPayment/Authenticate using password.png
    click element    class:submit
    log to console    Passcode Authenticated Successfully



