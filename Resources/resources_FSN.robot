*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${url}  https://myadminqa.int.unisa.ac.za/services/forgot-studentnumber-app/
${browser}  headlessfirefox
${surname}  Ming Sun
${names}    Jason
${dob}      1975-05-06
${submit}   /html/body/div/unisa-root/unisa-forgot-student-search/div/form/div[7]/button
${expected_Text}    Your Unisa student number is 34105700
${back}     /html[1]/body[1]/div[1]/unisa-root[1]/unisa-forgot-student-result[1]/div[1]/div[3]/div[1]/button[1]

*** Keywords ***
Launch Browser
    open browser    ${url}  ${browser}
    maximize browser window
    capture page screenshot    screenshots/ForgotStuNumber/landing page.png
    set selenium speed    5

Populate Student Info
    sleep   10
    input text  id:surname  ${surname}
    log to console    Add Surname
    input text  id:firstNames   ${names}
    log to console    Add First Names
    input text  id:dateOfBirth  ${dob}
    log to console    Add Date of Birth
    click element    xpath:/html[1]/body[1]/div[1]/unisa-root[1]/unisa-forgot-student-search[1]/div[1]/form[1]/div[3]/div[1]/div[1]/span[1]/button[1]/span[1]
    capture page screenshot    screenshots/ForgotStuNumber/Populate Student Info.png
    sleep    2

Click Submit
    click element    xpath:${submit}
    log to console    Click Submit

Verify Student Number Output
    set selenium implicit wait    30
    wait until page contains    ${expected_Text}
    log to console    Verify Student Number
    capture page screenshot    screenshots/ForgotStuNumber/Verify Student Number Output.png

Click Back Button
    set selenium implicit wait    20 seconds
    click element    xpath:${back}
    log to console    Click Back to test if the Back Button works
    sleep    2



