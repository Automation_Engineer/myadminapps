*** Settings ***
Library    SeleniumLibrary
Resource    ../Resources/resources_CCP.robot

*** Test Cases ***
Access CreditCard Tool
    [Setup]    Launch Browser
    Access Credit Card Payment App
    Complete the Personal and Payment Details
    Capture Card Details
    #Authenticate using password
    [Teardown]    close browser
