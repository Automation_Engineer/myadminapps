*** Settings ***
Library    SeleniumLibrary
Resource    ../Resources/resources_FSN.robot

*** Test Cases ***
Viewing Student Number
    [Setup]    Launch Browser
    Populate Student Info
    Click Submit
    Verify Student Number Output
    Click Back Button
    [Teardown]    close browser